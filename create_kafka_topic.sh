#!/bin/sh

CONTAINER_ID=$(docker ps -qf "name=grestpio_kafka")

docker exec -it $CONTAINER_ID sh -c '/opt/bitnami/kafka/bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --topic grestpio-events --partitions 1 --replication-factor 1'
